# Issues-Reporter client

## Intro

This npm package is for interacting with the [issues-reporter service](https://bitbucket.org/jfabfab/issues-reporter#readme). It is used to report errors occuring in your microservices. The issues-reporter service can then persist details about errors and make them all available in one single place.


## Installation

````shell
$ npm install --save issues-reporter-client
````

## Sample usage

### Client initialization

```javascript
const { IssuesReporterClient } = require('issues-reporter-client');

const client = new IssuesReporterClient({
  endpoint: 'http://localhost:5200', // optional (default: dev server), issue-reporter-service endpoint
  serviceName: 'test-service-name',            // required (the name of your service)
  serviceVersion: 'test-service-version',            // required (the version of your service)
  retryCount: 6,            // optional
  logger: true,             // optional (true, false, winston)
});
```
You can then send any issue to the issues-reporter service

````javascript
try {
  // do something
} catch (e) {
  const response = await client.reportIssue(e);
  // do something with response (status)
}
````

