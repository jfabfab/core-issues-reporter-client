/* global expect loggerForTests */


const proxyquire = require('proxyquire');
const { EventEmitter } = require('events');

describe('test suite for IssuesReporterClient', () => {
  let IssuesReporterClient;
  let issuesReporterClientArgs;
  let requestMock;

  beforeEach(() => {
    requestMock = new EventEmitter();
    requestMock.post = jasmine.createSpy('post').and.callFake(() => {
      setImmediate(() => {
        const responseMock = new EventEmitter();
        requestMock.emit('response', responseMock);
        responseMock.emit('data', JSON.stringify({ statusCode: 200 }));
        responseMock.emit('end');
        // requestMock.emit('response', { statusCode: 200 }, 'testResponse');
      });
      return requestMock;
    });
    requestMock.get = jasmine.createSpy('get').and.callFake((url, cb) => cb(null, { statusCode: 200 }, '{ "data": "testResponse" }'));

    IssuesReporterClient = proxyquire('./IssuesReporterClient', {
      request: requestMock,
    }).IssuesReporterClient;

    issuesReporterClientArgs = {
      endpoint: 'http://test.com',
      serviceName: 'testService',
      serviceVersion: 'testVersion',
      logger: loggerForTests,
      retryCount: 1,
    };
  });

  describe('constructor', () => {
    describe('required values', () => {
      function entry({ endpoint = 'http://test.com', serviceName = 'testService', serviceVersion = 'testVersion', retryCount, expectedErr }) {
        // prepare
        const args = Object.assign({}, issuesReporterClientArgs, {
          endpoint,
          serviceName,
          serviceVersion,
          retryCount,
        });

        // run
        function run() {
          return new IssuesReporterClient(args);
        }

        // result
        expect(() => run()).toThrowError(expectedErr);
      }

      it('should handle when serviceName is not passed', () => entry({
        serviceName: '',
        expectedErr: /serviceName/,
      }));

      it('should handle when serviceVersion is not passed', () => entry({
        serviceVersion: '',
        expectedErr: /serviceVersion/,
      }));

      it('should handle when retryCount is less than 1', () => entry({
        retryCount: 0,
        expectedErr: /retryCount/,
      }));
    });

    describe('default values', () => {
      function entry({ overrideArgs, result }) {
        // prepare
        const args = Object.assign({}, issuesReporterClientArgs, overrideArgs);

        // run
        const client = new IssuesReporterClient(args);

        // result
        result(client);
      }

      it('should support default retry', () => entry({
        overrideArgs: {
          retryCount: undefined,
        },
        result: (client) => {
          expect(client.retryCount).toEqual(5);
        },
      }));
    });
  });

  describe('reporting errors to service endpoint', () => {
    it('should handle error result', (done) => {
      // prepare
      const testErr = new Error('testErr');
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        setImmediate(() => {
          requestMock.emit('error', testErr);
        });
        return requestMock;
      });

      const client = new IssuesReporterClient(issuesReporterClientArgs);

      // run
      function run() {
        const errorMock = { test: true };
        return client.reportIssue(errorMock);
      }

      // result
      function test(err) {
        expect(err).toEqual(testErr);
      }

      Promise.resolve()
        .then(() => run())
        // .then(() => fail('expected to throw'))
        .catch(err => test(err))
        .then(() => done());
    });

    it('should handle notice of error sent in result', (done) => {
      // prepare
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        const responseMock = new EventEmitter();
        setImmediate(() => {
          requestMock.emit('response', responseMock);
          responseMock.emit('data', JSON.stringify({ errorMessage: 'error-message-test' }));
          responseMock.emit('end');
        });
        return requestMock;
      });

      const client = new IssuesReporterClient(issuesReporterClientArgs);

      // run
      function run() {
        const errorMock = { test: true };
        return client.reportIssue(errorMock);
      }

      // result
      function test(result) {
        const expectedResult = jasmine.objectContaining({
          errorMessage: jasmine.anything(),
        });
        expect(result).toEqual(expectedResult);
      }

      Promise.resolve()
        .then(() => run())
        .then(result => test(result))
        .then(() => done());
    });

    it('should handle result with no error notification', (done) => {
      // prepare
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        const responseMock = new EventEmitter();
        setImmediate(() => {
          requestMock.emit('response', responseMock);
          responseMock.emit('data', JSON.stringify({ statusCode: 200 }));
          responseMock.emit('end');
        });
        return requestMock;
      });

      const client = new IssuesReporterClient(issuesReporterClientArgs);

      // run
      function run() {
        const errorMock = { test: true };
        return client.reportIssue(errorMock);
      }

      // result
      function test(result) {
        const expectedResult = jasmine.objectContaining({
          statusCode: jasmine.anything(),
        });
        expect(result).toEqual(expectedResult);
      }

      Promise.resolve()
        .then(() => run())
        .then(result => test(result))
        .then(() => done());
    });

    it('should handle no result returned', (done) => {
      // prepare
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        const responseMock = new EventEmitter();
        setImmediate(() => {
          requestMock.emit('response', responseMock);
          // responseMock.emit('data');
          responseMock.emit('end');
        });
        return requestMock;
      });

      const client = new IssuesReporterClient(issuesReporterClientArgs);

      // run
      function run() {
        const errorMock = { test: true };
        return client.reportIssue(errorMock);
      }

      // result
      function test(result) {
        expect(result).not.toBeDefined();
      }

      Promise.resolve()
        .then(() => run())
        .then(result => test(result))
        .then(() => done());
    });

    describe('different args', () => {
      function entry({ done, call, expectedCall }) {
        // prepare
        const client = new IssuesReporterClient(issuesReporterClientArgs);

        // run
        function run() {
          return call(client);
        }

        // result
        function test() {
          expect(requestMock.post).toHaveBeenCalledWith(expectedCall);
        }

        Promise.resolve()
          .then(() => run())
          .then(() => test())
          .catch(err => fail(err))
          .then(() => done());
      }

      const errorMock = new Error('test-error');
      const expectedBody = {
        endpointParams: {
          service: 'issues',
          action: 'report',
          target: 'internal',
        },
        payload: {
          stack: jasmine.anything(),
          details: {
            service: 'testService',
            version: 'testVersion',
            error: JSON.stringify(errorMock),
          },
        },
      };

      it('should call reportIssue with the right args', done => entry({
        done,
        call: client => client.reportIssue(errorMock),
        expectedCall: jasmine.objectContaining({
          url: 'http://test.com',
          method: 'POST',
          json: expectedBody,
        }),
      }));
    });

    it('should not call when disabled', (done) => {
      // prepare
      issuesReporterClientArgs.endpoint = ''; // disable
      const client = new IssuesReporterClient(issuesReporterClientArgs);

      // run
      function run() {
        return client.reportIssue({
          test: 'testError',
        });
      }

      // result
      function test() {
        expect(client.enabled).toBe(false);
        expect(requestMock.post).not.toHaveBeenCalled();
      }

      Promise.resolve()
        .then(() => run())
        .then(() => test())
        .catch(err => fail(err))
        .then(() => done());
    });
  });
});
