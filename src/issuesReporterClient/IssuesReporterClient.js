const assert = require('assert');
const retry = require('bluebird-retry');
const request = require('request');
// const requestPromiseNative = require('request-promise-native');

/* istanbul ignore next: consoleLogger */
const consoleLogger = {
  debug: console.log,
  log: console.log,
  info: console.info,
  warn: console.warn,
  error: console.error,
};

/* istanbul ignore next: silentLogger */
const silentLogger = {
  debug: () => {},
  log: () => {},
  info: () => {},
  warn: () => {},
  error: console.error,
};

class IssuesReporterClient {
  /**
   * Creates an instance of IssuesReporterClient.
   * @param {any} { endpoint, serviceName, serviceVersion, logger, retryCount = 5 }
   *
   * @memberof IssuesReporterClient
   */
  constructor({
    endpoint,
    serviceName,
    serviceVersion,
    logger,
    retryCount = 5,
  }) {
    assert(serviceName, 'IssuesReporterClient expects serviceName');
    assert(serviceVersion, 'IssuesReporterClient expects serviceVersion');
    assert(retryCount >= 1, 'IssuesReporterClient expects retryCount to be greater than or equal to 1');

    this.enabled = !!endpoint;

    this.endpoint = endpoint;
    this.serviceName = serviceName;
    this.serviceVersion = serviceVersion;
    this.retryCount = retryCount;

    /* istanbul ignore next: logging */
    if (logger === true) {
      this.logger = consoleLogger;
    } else if (logger) {
      this.logger = logger;
    } else {
      this.logger = silentLogger;
    }
  }

  /**
   * Report errors to the provided endpoint
   * @param {any} error - instance of Error
   * @returns {any}
   */
  reportIssue(error) {
    if (!this.enabled) {
      return Promise.resolve();
    }

    const req = {
      url: this.endpoint, // `${apigatewayURI}/issues`
      method: 'POST',
      json: this._formatBody(error),
    };

    const retryOptions = {
      throw_original: true,
      context: this,
      interval: 1000,
      backoff: 2,
      max_tries: this.retryCount,
    };

    let retryCounter = 0;

    return Promise.resolve()
      .then(() =>
        retry(() => new Promise((resolve, reject) => {
          retryCounter += 1;
          this.logger.debug(`calling issues-reporter-service '${req.url}' (retry #${retryCounter})`);

          let data = '';

          request
            .post(req)
            .on('response', (response) => {
              let body;

              response.on('data', (chunk) => {
                // compressed data as it is received
                data += chunk;
              });

              response.on('end', () => {
                if (data) {
                  body = JSON.parse(data);
                }
                if (body && body.errorMessage) {
                  this.logger.error(`Failed to report this issue. ${body.errorMessage}`);
                } else if (body) {
                  this.logger.debug(`Sent to issues-reporter-service (status ${response.statusCode}) ${JSON.stringify(body)}`);
                }
                resolve(body);
              });
            })
            .on('error', (err) => {
              reject(err);
            });
        }), retryOptions))
      .catch((e) => {
        this.logger.error(`An error occured and we were unable to report the issue. ${e.message}`);
      });
  }

  /**
   * format body content
   * @param {any} error
   * @returns {any} body
   */
  _formatBody(error) {
    const body = {
      endpointParams: {
        service: 'issues',
        action: 'report',
        target: 'internal',
      },
      payload: {
        stack: error.stack,
        details: {
          service: this.serviceName,
          version: this.serviceVersion,
          error: JSON.stringify(error),
        },
      },
    };

    return body;
  }
}

module.exports = {
  IssuesReporterClient,
};
