const { IssuesReporterClient } = require('./src/issuesReporterClient/IssuesReporterClient');

module.exports = {
  IssuesReporterClient,
};
